<?php
/**
*
* Controller
* 
* @author Gonzalo Sanchez Cano
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the controller framework
jimport('joomla.application.component.controller');

if(!class_exists('VmController'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmcontroller.php');


class VirtuemartControllerpurchaseorders extends VmController {

	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');

	}

	public function getProduct(){
		$modelPurchaseorders = VmModel::getModel("purchaseorders");
		echo $modelPurchaseorders->getProduct();
	}


	function save($data = 0){
		$res = parent::save($data = 0);
		$modelPurchaseorders = VmModel::getModel("purchaseorders");
		$modelPurchaseorders->getSaveProducts($data = 0);
		//var_dump($res, $data, $this, $_POST, $_GET);
		//die();
		return $res;
	}   

	function remove($data = 0){
		$modelPurchaseorders = VmModel::getModel("purchaseorders");
		$modelPurchaseorders->removeProducts();
		return parent::remove();
	}
	
}
// pure php no closing tag
