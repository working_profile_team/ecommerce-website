<?php
/**
*
* controller
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the controller framework
jimport('joomla.application.component.controller');

if(!class_exists('VmController'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmcontroller.php');


class VirtuemartControllerpurchaseorderspaymentreport extends VmController {

	/**
	 * Method to display the view
	 *
	 * @access	public
	 * @author
	 */
	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');

	}

	//opcion llamada desde el boton
	public function updateStatus() {
		JRequest::checkToken() or jexit( 'Invalid Token save' );

		$mainframe = Jfactory::getApplication();

		// Load the view object 
		$view = $this->getView('stockentry', 'html');

		//  Load the model
		$model = VmModel::getModel('purchaseorderspayment');
		
		// getting the order
		$payments = JRequest::getVar('virtuemart_purchaseorders_id');
					
		$model->updateStockStatus($payments);
		$msg = JText::_('COM_VIRTUEMART_PURCHASE_ORDER_PAYMENT_UPDATED_SUCCESSFULLY' );
		$mainframe->redirect('index.php?option=com_virtuemart&view=purchaseorderspayment', $msg);
	}
	
	
}
// pure php no closing tag
