<?php
/**
*
* controller
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the controller framework
jimport('joomla.application.component.controller');

if(!class_exists('VmController'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmcontroller.php');


class VirtuemartControllerpurchaseordersadmin extends VmController {

	/**
	 * Method to display the view
	 *
	 * @access	public
	 * @author
	 */
	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');

	}

/*
	function edit($view=0){

		//We set here the virtuemart_user_id, when no virtuemart_user_id is set to 0, for adding a new user
		//In every other case the virtuemart_user_id is sent.
		$cid = JRequest::getVar('virtuemart_purchaseorders_id');
		if(!isset($cid)) JRequest::setVar('virtuemart_purchaseorders_id', (int)0);
		//var_dump($this);
		//die();
		parent::edit('edit');
	}
*/

}
// pure php no closing tag
