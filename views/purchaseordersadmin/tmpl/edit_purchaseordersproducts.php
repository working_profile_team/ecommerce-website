<?php
/**
*
* View Edit Images
*
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access'); ?>


	<div style="text-align: left;">
	<fieldset>
		<legend><?php echo JText::_('COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_PRODUCTS_LBL'); ?></legend>
		<table class="admintable">
			<?php echo VmHTMLExtension::row('input','COM_VIRTUEMART_PURCHASE_ORDER_IMPORT_COST_TOTAL','import_cost_total_with_currency', $this->onePurchaseorder->po_import_cost_total.' '.$this->onePurchaseorder->po_currency , '', 'readonly="true"'); ?>
			<input type="hidden" id="po_import_cost_total" name="po_import_cost_total" value="<?php echo $this->onePurchaseorder->po_import_cost_total ?>" />
			<input type="hidden" id="po_currency" name="po_currency" value="<?php echo $this->onePurchaseorder->po_currency ?>" />
		</table>
		<table class="adminlist" cellspacing="0" cellpadding="0" id="purchaseordersEditTable">
		<thead>
		<tr>
			<th><?php echo $this->sort('product_name') ?></th>
			<th><?php echo $this->sort('product_sku')?></th>
			<th><?php echo $this->sort('orderedquantity') ?></th>
			<th><?php echo $this->sort('product_in_stock_central') ?></th>
		</tr>
		</thead>
		<tbody>
			
		<?php
		if (count($this->productsList) > 0) {
			$i = 0;
			$k = 0;
			//~ $keyword = JRequest::getWord('keyword');
			foreach ($this->productsList as $key => $product) {
				$published = JHTML::_('grid.published', $product, $i );
				
				//<!-- receivedquantity_is_less_than_orderedquantity_notification  -->
				if ( $product->product_in_stock_central - $product->orderedquantity < 0) $receivedquantitystatus ="out";
				else $receivedquantitystatus = "normal";


				
				//La clase la toma del archivo /com_virtuemart/assets/css/admin.styles.css, cargado 
				//por la clase /com_virtuemart/helpers/adminui.php
				$receivedquantitystatus='class="stock-'.$receivedquantitystatus.'" title="'.jText::_('COM_VIRTUEMART_PURCHASE_ORDER_RECEIVEDQUANTITY_LEVEL_'.$receivedquantitystatus).'"';
				?>
				<tr class="row<?php echo $k ; ?>">
					<!-- Product name -->
					<?php
					$link = 'index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'&product_parent_id='.$product->product_parent_id;
					?>
					<td><?php echo JHTML::_('link', JRoute::_($link), $product->product_name, array('title' => JText::_('COM_VIRTUEMART_EDIT').' '.$product->product_name)); ?></td>
					<td><?php echo $product->product_sku; ?></td>
					<td <?php echo $receivedquantitystatus; ?> width="15%"><?php echo $product->orderedquantity; ?></td>
					<td width="15%"><?php echo $product->product_in_stock_central; ?></td>
				</tr>
			<?php
				
				$k = 1 - $k;
				$i++;
			}
		}
		?>
		
		</tbody>
		</table>
	</fieldset>
</div>
