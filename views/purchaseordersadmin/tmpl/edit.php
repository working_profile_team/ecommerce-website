<?php
/**
*
* View Edit
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

AdminUIHelper::startAdminArea();

?>


<form method="post" id="adminForm" name="adminForm" action="index.php" enctype="multipart/form-data" >


<?php 
$tabarray = array();
$tabarray['info'] = 'COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_INFO_LBL';
$tabarray['purchaseordersproducts'] = 'COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_PRODUCTS_LBL';
AdminUIHelper::buildTabs ( $this,  $tabarray ,$this->onePurchaseorder->virtuemart_purchaseorders_id);
// Loading Templates in Tabs END ?>
<?php echo $this->addStandardHiddenToForm(); ?>	
<input type="hidden" name="virtuemart_purchaseorders_id" value="<?php echo $this->onePurchaseorder->virtuemart_purchaseorders_id; ?>" />

	

</form>


<?php AdminUIHelper::endAdminArea(); ?>
