<?php
/**
*
* View Edit Description
*/


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>





<div class="col50">
	<fieldset>
	<legend><?php echo JText::_('COM_VIRTUEMART_PURCHASE_ORDER_DETAILS'); ?></legend>
	<table class="admintable">
		<?php echo VmHTMLExtension::row('input','COM_VIRTUEMART_PURCHASE_ORDER_NAME','po_name',$this->onePurchaseorder->po_name, $readonly='readonly="true"'); ?>
		<?php echo VmHTMLExtension::row('textarea','COM_VIRTUEMART_PURCHASE_ORDER_DESCRIPTION','po_desc',$this->onePurchaseorder->po_desc, $extraAttribs = 'readonly="true"'); ?>
		<?php echo VmHTML::row('select','COM_VIRTUEMART_PURCHASE_ORDER_STATE','po_stateid',$this->purchaseordersstateList,$this->onePurchaseorder->po_stateid,'','virtuemart_purchaseordersstate_id', 'pot_name',false); ?>
	</table>
	</fieldset>
</div>

