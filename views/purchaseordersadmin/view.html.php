
<?php
/**
*
* View
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the view framework
if(!class_exists('VmView'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmview.php');

class VirtuemartViewPurchaseordersadmin extends VmView {

	function display($tpl = null) {

		// Load the helper(s)
		$this->loadHelper('html');
		$this->loadHelper('htmlextension');

		// get necessary model of Purchaseorders
		$modelPurchaseorders = VmModel::getModel();
		$onePurchaseorder = $modelPurchaseorders->getOnePurchaseorders();
		$this->SetViewTitle('PURCHASE_ORDER_ADMIN');

     	$layoutName = JRequest::getWord('layout', 'default');
     	$layoutName = $this->getLayout();
		if ($layoutName == 'edit') {
			
			
			//get the Purchaseorders data filter by virtuemart_purchaseorders_id
			
			$this->assignRef('onePurchaseorder',	$onePurchaseorder);
			//add standars buttoms
			$this->addStandardEditViewCommands($modelPurchaseorders->virtuemart_purchaseorders_id);

			//var_dump($_POST);
			//die();

			
			//get all the ProductsList filter by PurchaseOrder
			$productsList = $modelPurchaseorders->getProductsListByPurchaseOrder(false,false);
			$this->assignRef('productsList', $productsList);
			//get all the ProductsList filter by PurchaseOrder
			$purchaseordersstateList = $modelPurchaseorders->getPurchaseordersstateList();
			$this->assignRef('purchaseordersstateList', $purchaseordersstateList);
			
			$categoryModel = VmModel::getModel('manufacturercategories');
			$manufacturerCategories = $categoryModel->getManufacturerCategories(false,true);
			$this->assignRef('manufacturerCategories',	$manufacturerCategories);
			//get the Purchaseorders pagination
			$pagination = $modelPurchaseorders->getPagination();
			$this->assignRef('pagination', $pagination);

        }
        else {
			//add standars commands and orders
        	//$this->addStandardDefaultViewCommands();
   			JToolBarHelper::divider();
			//JToolBarHelper::publishList();
			//JToolBarHelper::unpublishList();
			JToolBarHelper::editListX();
			//JToolBarHelper::addNewX();
			//JToolBarHelper::deleteList();
			$this->addStandardDefaultViewLists($modelPurchaseorders);
			//get all the purchaseOrders
			$purchaseOrders = $modelPurchaseorders->getPurchaseOrders();
			$this->assignRef('purchaseOrders',	$purchaseOrders);
			//get the Purchaseorders pagination
			$pagination = $modelPurchaseorders->getPagination();
			$this->assignRef('pagination', $pagination);
			//$this->assignRef('virtuemart_purchaseorders_id', $modelPurchaseorders->virtuemart_purchaseorders_id);

		}
		parent::display($tpl);
	}

}
// pure php no closing tag


