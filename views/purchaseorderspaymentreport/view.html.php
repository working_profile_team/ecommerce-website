<?php
/**
*
* View
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the view framework
if(!class_exists('VmView'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmview.php');

class VirtuemartViewPurchaseorderspaymentreport extends VmView {

	function display($tpl = null) {
		// Load the helper(s)
		$this->loadHelper('html');

		// get necessary model
		$model = VmModel::getModel();

		$this->SetViewTitle('PURCHASE_ORDER_PAYMENT_REPORT');
		
     	$layoutName = JRequest::getWord('layout', 'default');
		if ($layoutName == 'default') {
        	//JToolBarHelper::save('updateStatus', JText::_('COM_VIRTUEMART_STOCK_ENTRY_UPDATE_RECEIVED'));

			$purchaseOrders = $model->getPurchaseOrders();
			$this->assignRef('purchaseOrders',	$purchaseOrders);

			$pagination = $model->getPagination();
			$this->assignRef('pagination', $pagination);

		}
		parent::display($tpl);
	}

}
// pure php no closing tag
