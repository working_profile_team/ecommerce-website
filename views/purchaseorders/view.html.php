<?php
/**
*
* View
* 
* @author Gonzalo Sanchez Cano
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the view framework
if(!class_exists('VmView'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmview.php');

class VirtuemartViewPurchaseorders extends VmView {

	function display($tpl = null) {

		// Load the helper(s)
		$this->loadHelper('html');
		$this->loadHelper('htmlextension');

		// get necessary model of Purchaseorders
		$modelPurchaseorders = VmModel::getModel();

		$this->SetViewTitle('PURCHASE_ORDER');

     	$layoutName = JRequest::getWord('layout', 'default');
		if ($layoutName == 'edit') {
			
			
			//get the Purchaseorders data filter by virtuemart_purchaseorders_id
			$dataPurchaseorders = $modelPurchaseorders->getData();
			$this->assignRef('onePurchaseorder',	$dataPurchaseorders);
			//add standars buttoms
			$this->addStandardEditViewCommands($dataPurchaseorders->virtuemart_purchaseorders_id);
			//get all the ProductsList filter by PurchaseOrder
			$productsList = $modelPurchaseorders->getProductsListByPurchaseOrder(false,false);
			$this->assignRef('productsList', $productsList);
			
			$availableProductsList = $modelPurchaseorders->getAvailableProductsList(false,false);
			$this->assignRef('availableProductsList', $availableProductsList);
			
			//get the Purchaseorders pagination
			$pagination = $modelPurchaseorders->getPagination();
			$this->assignRef('pagination', $pagination);

        }
        else {
			//add commands
   			JToolBarHelper::divider();
			JToolBarHelper::editListX();
			JToolBarHelper::addNewX();
			JToolBarHelper::deleteList();
        	$this->addStandardDefaultViewLists($modelPurchaseorders);
			//get all the purchaseOrders
			$purchaseOrders = $modelPurchaseorders->getPurchaseOrders();
			$this->assignRef('purchaseOrders',	$purchaseOrders);
			//get the Purchaseorders pagination
			$pagination = $modelPurchaseorders->getPagination();
			$this->assignRef('pagination', $pagination);

		}
		parent::display($tpl);
	}
	
	


}
// pure php no closing tag


