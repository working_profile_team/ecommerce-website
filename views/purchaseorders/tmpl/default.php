<?php
/**
*
* View Default
* 
* @author Gonzalo Sanchez Cano
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

AdminUIHelper::startAdminArea();

?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<div id="editcell">
		<table class="adminlist" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<th width="10">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->purchaseOrders); ?>);" />
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_NAME'); ?>
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_DESCRIPTION'); ?>
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_STATE'); ?>
			</th>
			<th>
				<?php echo $this->sort('virtuemart_purchaseorders_id', 'COM_VIRTUEMART_ID')  ?>
			</th>
		</tr>
		</thead>
		<?php
		$k = 0;
		for ($i=0, $n=count( $this->purchaseOrders ); $i < $n; $i++) {
			$row = $this->purchaseOrders[$i];

			$checked = JHTML::_('grid.id', $i, $row->virtuemart_purchaseorders_id,null,'virtuemart_purchaseorders_id');
			$published = JHTML::_('grid.published', $row, $i);
			$editlink = JROUTE::_('index.php?option=com_virtuemart&view=purchaseorders&task=edit&virtuemart_purchaseorders_id=' . $row->virtuemart_purchaseorders_id);
			$manufacturersList = JROUTE::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_purchaseorders_id=' . $row->virtuemart_purchaseorders_id);

			?>
			<tr class="row<?php echo $k ; ?>">
				<td width="10">
					<?php echo $checked; ?>
				</td>
				<td align="left">
					<a href="<?php echo $editlink; ?>"><?php echo $row->po_name; ?></a>

				</td>
				<td>
					<?php echo JText::_($row->po_desc); ?>
				</td>
				<td>
					<?php echo JText::_($row->po_state); ?>
				</td>
				<td align="right">
					<?php echo $row->virtuemart_purchaseorders_id; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
	<?php echo $this->addStandardHiddenToForm(); ?>
</form>


<?php AdminUIHelper::endAdminArea(); ?>
