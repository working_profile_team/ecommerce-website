<?php
/**
*
* View Edit Info
* 
* @author Gonzalo Sanchez Cano 
*/


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

?>





<div class="col50">
	<fieldset>
	<legend><?php echo JText::_('COM_VIRTUEMART_PURCHASE_ORDER_DETAILS'); ?></legend>
	<table class="admintable">
		<?php echo VmHTML::row('input','COM_VIRTUEMART_PURCHASE_ORDER_NAME','po_name',$this->onePurchaseorder->po_name); ?>
		<?php echo VmHTML::row('textarea','COM_VIRTUEMART_PURCHASE_ORDER_DESCRIPTION','po_desc',$this->onePurchaseorder->po_desc); ?>
	</table>
	</fieldset>
	<input type="hidden" name="po_stateid" value="4" />
	<input type="hidden" name="published" value="1" />
</div>

