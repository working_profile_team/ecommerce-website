<?php
/**
*
* View Edit
* 
* @author Gonzalo Sanchez Cano
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

AdminUIHelper::startAdminArea();

?>


<form enctype="multipart/form-data" action="index.php" method="post" name="adminForm" id="adminForm">


<?php 
$tabarray = array();
$tabarray['info'] = 'COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_INFO_LBL';
$tabarray['purchaseordersproducts'] = 'COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_PRODUCTS_LBL';
AdminUIHelper::buildTabs ( $this,  $tabarray ,$this->onePurchaseorder->virtuemart_purchaseorders_id);
// Loading Templates in Tabs END ?>
<input type="hidden" name="virtuemart_purchaseorders_id" value="<?php echo $this->onePurchaseorder->virtuemart_purchaseorders_id; ?>" />

<?php echo $this->addStandardHiddenToForm(); ?>	

	

</form>


<?php
AdminUIHelper::endAdminArea(); ?>
