<?php
/**
*
* View Edit Images
*
* @author Gonzalo Sanchez Cano
*/


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access'); ?>

	<script type="text/javascript">
		var globals = {
			text_edit: "<?php echo JText::_('COM_VIRTUEMART_EDIT') ?>",
			currency: "R$",
			delete_text:'Ingresar una cantidad de 0 eliminará el producto, ¿desea continuar?',
		};
		var products = [];
	</script>
	<div style="text-align: left;">
	<fieldset>
		<legend><?php echo JText::_('COM_VIRTUEMART_PURCHASE_ORDER_FORM_PURCHASE_ORDER_PRODUCTS_LBL'); ?></legend>
		<table class="admintable">
			<?php echo VmHTML::row('select','COM_VIRTUEMART_PURCHASE_ORDER_STATE','p_product_sku',$this->availableProductsList,'','','virtuemart_product_id', 'product_sku',false); ?>
			<?php echo VmHTMLExtension::row('input','COM_VIRTUEMART_PURCHASE_ORDER_IMPORT_COST_TOTAL','import_cost_total_with_currency', '0', '', 'readonly="true"'); ?>
			<input type="hidden" id="po_import_cost_total" name="po_import_cost_total" value="0" />
			<input type="hidden" id="po_currency" name="po_currency" value="" />
		</table>
		<table class="adminlist" cellspacing="0" cellpadding="0" id="purchaseordersEditTable">
			<thead>
			<tr id="linea" class="row1">
				<th><?php echo $this->sort('product_name') ?></th>
				<th><?php echo $this->sort('product_sku')?></th>
				<th><?php echo $this->sort('product_import_cost')?></th>
				<th><?php echo $this->sort('orderedquantity') ?></th>
				<th><?php echo $this->sort('orderedquantitytotal') ?></th>
				<th><?php echo $this->sort('product_in_stock_central') ?></th>
				<th><?php echo Delete ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
				foreach ($this->productsList as $key => $product) {
			?>
					<script type="text/javascript">
						products.push({
							id: <?php echo $product->virtuemart_product_id; ?>,
							sku: "<?php echo $product->product_sku; ?>",
							edit_link: "<?php echo 'index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='.$product->virtuemart_product_id.'&product_parent_id='.$product->product_parent_id; ?>",
							name: "<?php echo $product->product_name; ?>",
							//precio: <?php echo number_format( $product->orderedquantity*$product->product_import_cost, 4); ?>,
							currency_symbol: "<?php echo $product->currency_symbol; ?>",
							import_cost: "<?php echo number_format($product->product_import_cost, 4); ?>",
							stock_central: "<?php echo $product->product_in_stock_central; ?>",
							import_cost_subtotal: "<?php echo number_format($product->orderedquantity*$product->product_import_cost, 4) ?>", 
							orderedquantity: "<?php echo$product->orderedquantity ?>"	,
						});
					</script>
			<?php	
				}
			?>

				
			</tbody>
		</table>
	</fieldset>
	
	<input type="hidden" id="pop_lista" name="pop_lista" value="4" />
</div>
<div id="prueba"></p>
<script type="text/javascript">
	
	window.addEvent('domready', init );
	
	//Accion al seleccionar el combo
	function product_selected(e){
		var comboId = jQuery("#p_product_sku option:selected").val();
		var isNotValid = jQuery("#purchaseordersEditTable input[name='products-virtuemart_product_id[]'][value="+comboId+"]").length > 0;
		if (isNotValid) {
			alert("No se puede seleccionar dos veces el mismo producto");
		}else{

			jQuery.ajax({
				type: "POST",
				url: "index.php?option=com_virtuemart&controller=purchaseorders&format=raw&task=getProduct",
				data: { 
						product_id: jQuery("#p_product_sku").val()
				},
				success: function(datainterna) {
					var data = eval( datainterna );	
					var product = {
						id: data[0].virtuemart_product_id,
						sku: data[0].product_sku,
						edit_link: 'index.php?option=com_virtuemart&view=product&task=edit&virtuemart_product_id='+data[0].virtuemart_product_id+'&product_parent_id='+data[0].virtuemart_product_id,
						name: data[0].product_name,
						//precio: <?php echo number_format( $product->orderedquantity*$product->product_import_cost, 4); ?>,
						currency_symbol: data[0].currency_symbol,
						import_cost: data[0].product_import_cost,
						stock_central: data[0].product_in_stock_central,
						import_cost_subtotal: parseFloat(data[0].product_import_cost).toFixed(4).toString(),
						orderedquantity: 1,
					};
					drawTableRow(product);
					actionUpdateOrderedquantitystatus();
					actionUpdatePurchaseorderimportcosttotal();
					
					
				},
				error:function (xhr, ajaxOptions, thrownError){
					alert("Joomla Platform Error Status: " + xhr.status + " Thrown Errors: "+thrownError);
				}
			});
		}



	}
					
				
	//Ingreso de fila en la tabla
	function drawTableRow(productObject) {
		var orderedquantitystatus;
		var lastClassIndex = jQuery('#purchaseordersEditTable tr:last').attr('class').replace('row','').replace(' ','');
		classIndex = 0;
		if (lastClassIndex == 0)
			classIndex = 1;
		var number = parseFloat(productObject.import_cost).toFixed(4)+' '+productObject.currency_symbol;;
		var htmlTr = [
		'<tr id="'+productObject.sku+'" class="row'+classIndex+'">',
		'	<input type="hidden" name="products-virtuemart_product_id[]" value="'+productObject.id+'" />',
		'	<td><a href="'+productObject.edit_link+'" title="'+globals.text_edit+' '+productObject.name+'">'+productObject.name+'</a></td>',
		'	<td>'+productObject.sku+'<input type="hidden" name="products-product_sku[]" value="'+productObject.sku+'" /></td>',
		'	<td>'+number+'</td>',
		'</tr>' 
		];
		var tr = jQuery(htmlTr.join(' '));
		
		
		//products-orderedquantity cell creation
		var htmlTd_orderedQuantityRow = [
			'<td width="15%" ',
			'class="stock-normal" title="<?php echo jText::_("COM_VIRTUEMART_PURCHASE_ORDER_RECEIVEDQUANTITY_LEVEL_NORMAL") ?>">',
			'	<input type="text" name="products-orderedquantity[]" value="'+productObject.orderedquantity+'" stock_central="'+productObject.stock_central+'" ',
			'	import_cost="'+productObject.import_cost+'" currency="'+productObject.currency_symbol+'" />',
			'</td>'
		];
		
		var orderedQuantityRow = jQuery(htmlTd_orderedQuantityRow.join(' '));
		orderedQuantityRow.vm2admin('tips');
		orderedQuantityRow.children('input[type="text"]').change( actionDeleteIfOrderedquantityIsZero ); //click es una funcion de jQuery
		orderedQuantityRow.children('input[type="text"]').change( actionUpdateOrderedquantityProductimportcostsubtotal ); //click es una funcion de jQuery
		orderedQuantityRow.children('input[type="text"]').change( actionUpdateOrderedquantitystatus ); //click es una funcion de jQuery
		orderedQuantityRow.children('input[type="text"]').change( actionUpdatePurchaseorderimportcosttotal ); //click es una funcion de jQuery
		tr.append(orderedQuantityRow);
		
		
		//product_import_cost_subtotal cell creation
		var import_cost_subtotal_with_currency = productObject.import_cost_subtotal+' '+productObject.currency_symbol;
		var htmlTd_importCostTotalRow = [
			'<td width="15%" name="product_import_cost_subtotal">',
			'	<span>'+import_cost_subtotal_with_currency+'</span>',
			'	<input type="hidden" value="'+productObject.import_cost_subtotal+'" />',
			'</td>'
		];
		tr.append(jQuery(htmlTd_importCostTotalRow.join(' ')));
	
		
		//product_in_stock_central cell creation
		var stockCentralRow = jQuery('<td width="15%" name="product_in_stock_central">'+productObject.stock_central+'</td>');
		tr.append(stockCentralRow);
	
	
		//Delete cell creation
		//la funcion jQuery convierte o un elementoHTML o un String que representa a un elemento en un elemento Jquery
		//eso implica que es un elementoHTML "sobrecargado" con todas las funciones de jQuery
		var span = jQuery('<span class="vmicon vmicon-16-remove borrar" ></span>');
		span.click( actionDelete ); //click es una funcion de jQuery
		var deleteRow = jQuery('<td width="5%"></td>');
		deleteRow.append(span);
		//var td = jQuery( 'td:last', tr );
		tr.append( deleteRow );
						
		
		jQuery('#purchaseordersEditTable').append( tr );	
	}
	
	//Acciones llamadas por eventos
	function actionDelete(e){
		var removeElement = jQuery(this);
		removeElement.closest("tr").remove();
		actionUpdatePurchaseorderimportcosttotal();
	}
	
	function actionDeleteIfOrderedquantityIsZero(e){
		var removeElement = jQuery(this);
		if (removeElement.val()==0){
			if( confirm( globals.delete_text ) ){
				removeElement.closest("tr").remove();
				actionUpdatePurchaseorderimportcosttotal();
			} else {
				removeElement.val(1);
			}
		}
	}
	function actionUpdateOrderedquantityProductimportcostsubtotal(e){
		var input = jQuery(this);
		var import_cost_subtotal = (input.val()*input.attr("import_cost")).toFixed(4);
		var import_cost_subtotal_with_currency = import_cost_subtotal+' '+input.attr("currency");
		var td = input.closest("tr").find("td[name='product_import_cost_subtotal']");
		
		td.find("span").text(import_cost_subtotal_with_currency);
		td.find("input").val(import_cost_subtotal);		
	}
	
	
	//Acciones generales
	function init(){
		jQuery('#p_product_sku').chosen().change( product_selected );
		jQuery(products).each(function () {
			drawTableRow(this);
		});
		actionUpdateOrderedquantitystatus();
		actionUpdatePurchaseorderimportcosttotal();
	}
	
	function actionUpdateOrderedquantitystatus(){
		var input = jQuery('#purchaseordersEditTable tr input[type="text"]');
		input.each(function(){
			var cssClass = 'stock-normal';
			var titulo = '<?php echo jText::_('COM_VIRTUEMART_PURCHASE_ORDER_RECEIVEDQUANTITY_LEVEL_NORMAL') ?>';
			if( jQuery(this).val() > jQuery(this).attr('stock_central') ){
				 cssClass = 'stock-out';
				 titulo = '<?php echo jText::_('COM_VIRTUEMART_PURCHASE_ORDER_RECEIVEDQUANTITY_LEVEL_OUT') ?>';
			}
			jQuery(this).parent().attr("title", titulo);
			jQuery(this).parent().removeClass('stock-normal stock-out').addClass(cssClass);
		});
	}

	function actionUpdatePurchaseorderimportcosttotal(e){
		var sum = 0;
		var input = jQuery('#purchaseordersEditTable td[name="product_import_cost_subtotal"] input');
		input.each(function(){
			sum += parseFloat(jQuery(this).val());
		});
		sumFixed = sum.toFixed(4);
		var sum_with_currency = sumFixed+' '+globals.currency;
		
		jQuery('#po_import_cost_total').val(sumFixed);
		jQuery('#po_currency').val(globals.currency);
		jQuery('#import_cost_total_with_currency').val(sum_with_currency);
		
	}

</script>
