<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage Manufacturer Category
* @author Patrick Kohl
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 5628 2012-03-08 09:00:21Z alatak $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

AdminUIHelper::startAdminArea();

?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<div id="editcell">
		<table class="adminlist" cellspacing="0" cellpadding="0">
		<thead>
		<tr>
			<th width="10">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->purchaseOrders); ?>);" />
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_NAME'); ?>
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_DESCRIPTION'); ?>
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_IMPORT_COST_TOTAL'); ?>
			</th>
			<th>
				<?php echo  JText::_('COM_VIRTUEMART_PURCHASE_ORDER_STATE'); ?>
			</th>
			<th>
				<?php echo $this->sort('virtuemart_purchaseorders_id', 'COM_VIRTUEMART_ID')  ?>
			</th>
		</tr>
		</thead>
		<?php
		$k = 0;
		for ($i=0, $n=count( $this->purchaseOrders ); $i < $n; $i++) {
			$row = $this->purchaseOrders[$i];
			
			$checked = JHTML::_('grid.id', $i, $row->virtuemart_purchaseorders_id,null,'virtuemart_purchaseorders_id');
			
			?>
			<tr class="row<?php echo $k ; ?>">
				<td width="10">
					<?php echo $checked; ?>
				</td>
				<td align="left">
					<?php echo $row->po_name; ?>
				</td>
				<td>
					<?php echo JText::_($row->po_desc); ?>
				</td>
				<td>
					<?php echo $row->po_import_cost_total.' '.$row->po_currency ?>
				</td>
				<td>
					<?php echo JText::_($row->po_state); ?>
				</td>
				<td align="right">
					<?php echo $row->virtuemart_purchaseorders_id; ?>
				</td>
			</tr>
			<?php
			$k = 1 - $k;
		}
		?>
		<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
	</table>
</div>

	<?php echo $this->addStandardHiddenToForm(); ?>
</form>


<?php AdminUIHelper::endAdminArea(); ?>



