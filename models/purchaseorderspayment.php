<?php
/**
*
* model
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if(!class_exists('VmModel'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmmodel.php');

class VirtuemartModelPurchaseorderspayment extends VmModel {


	/**
	 * constructs a VmModel
	 * setMainTable defines the maintable of the model
	 * @author Max Milbers
	 */
	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');
		$this->setMainTable('purchaseorders');
		$this->addvalidOrderingFieldName(array('po_name'));
		$config=JFactory::getConfig();
	}


	/**
	 * Retireve a list of countries from the database.
	 *
     * @param string $onlyPuiblished True to only retreive the published categories, false otherwise
     * @param string $noLimit True if no record count limit is used, false otherwise
	 * @return object List of purchase orders objects
	 */
	function getPurchaseOrders($onlyPublished=false, $noLimit=false)
	{
		$this->_noLimit = $noLimit;
		$select = 'l.*, po.*, pot_name AS po_state FROM `#__virtuemart_purchaseorders_'.VMLANG.'` as l';
		$joinedTables  = ' JOIN `#__virtuemart_purchaseorders` as po using (`virtuemart_purchaseorders_id`)';
		$joinedTables .= ' JOIN `#__virtuemart_purchaseordersstate` as pos on pos.virtuemart_purchaseordersstate_id = po_stateid';
		$where = array();
		$where[] = ' l.po_stateid in (5) ';
		$whereString = '';
		if (count($where) > 0) $whereString = ' WHERE '.implode(' AND ', $where) ;
		if ( JRequest::getCmd('view') == 'purchaseordersadmin') {
			$ordering = $this->_getOrdering('po.');
		} else {
			$ordering = ' order by po_name DESC';
		}
		return $this->_data = $this->exeSortSearchListQuery(0,$select,$joinedTables,$whereString,$ordering);

	}

	
	/*funcion llamada desde el controller*/
	public function updateStockStatus($payments){
		$db = JFactory::getDBO();
		for($i = 0; $i < count($payments); $i++){
			$queryUpdatePO = 	"UPDATE #__virtuemart_purchaseorders_".VMLANG." 
								SET po_stateid = 7
								WHERE virtuemart_purchaseorders_id = $payments[$i]";
			$db->setQuery($queryUpdatePO);
			$db->query();
			
		}
		$this->gettingSQLErrors();
	}	
	
	/*funcion interna para dezplegar los errores de sql en la vista*/
	private function gettingSQLErrors() {

		$errMsg = $this->_db->getErrorMsg ();
		$errs = $this->_db->getErrors ();

		if (!empty($errMsg)) {
			$app = JFactory::getApplication ();
			$errNum = $this->_db->getErrorNum ();
			$app->enqueueMessage ('SQL-Error: ' . $errNum . ' ' . $errMsg);
		}

		if ($errs) {
			$app = JFactory::getApplication ();
			foreach ($errs as $err) {
				$app->enqueueMessage ($err);
			}
		}


	}
	
}

// pure php no closing tag



