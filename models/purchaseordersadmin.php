<?php
/**
*
* model
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if(!class_exists('VmModel'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmmodel.php');

class VirtuemartModelPurchaseordersadmin extends VmModel {


	/**
	 * constructs a VmModel
	 * setMainTable defines the maintable of the model
	 * @author Max Milbers
	 */
	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');
		$this->setMainTable('purchaseorders');
		$this->addvalidOrderingFieldName(array('po_name'));
		$config=JFactory::getConfig();
	}

	/**
	 * Delete all record ids selected
     *
     * @return boolean True is the remove was successful, false otherwise.
     */
	function remove($categoryIds)
	{
    	$table = $this->getTable('purchaseorders');

    	foreach($categoryIds as $categoryId) {
       		if($table->checkManufacturer($categoryId)) {
	    		if (!$table->delete($categoryId)) {
	            		vmError($table->getError());
	            		return false;
	       		}
       		}
       		else {
				vmError(get_class( $this ).'::remove '.$categoryId.' '.$table->getError());
       			return false;
       		}
    	}
    	return true;
	}


	function getPurchaseOrders($onlyPublished=false, $noLimit=false)
	{
		$this->_noLimit = $noLimit;
		$select = 'l.*, po.*, pot_name AS po_state FROM `#__virtuemart_purchaseorders_'.VMLANG.'` as l';
		$joinedTables  = ' JOIN `#__virtuemart_purchaseorders` as po using (`virtuemart_purchaseorders_id`)';
		$joinedTables .= ' JOIN `#__virtuemart_purchaseordersstate` as pos on pos.virtuemart_purchaseordersstate_id = po_stateid';
		$where = array();
		$where[] = ' l.po_stateid in (4) ';
		$whereString = '';
		if (count($where) > 0) $whereString = ' WHERE '.implode(' AND ', $where) ;
		if ( JRequest::getCmd('view') == 'purchaseordersadmin') {
			$ordering = $this->_getOrdering('po.');
		} else {
			$ordering = ' order by po_name DESC';
		}
		return $this->_data = $this->exeSortSearchListQuery(0,$select,$joinedTables,$whereString,$ordering);

	}


		function getProductsListByPurchaseOrder ($publishedOnly = FALSE) {
		//get the ID
		$ID = $this->_id;
		//var_dump($this->_id);
		//die();
		// get products
		$q = 	'SELECT IFNULL((SELECT orderedquantity ';
		$q .= ' 		FROM #__virtuemart_purchaseordersproducts AS item ';
		$q .= ' 		WHERE item.virtuemart_purchaseorders_id = '.$ID.' AND ';
		$q .= ' 		item.virtuemart_product_id = p.virtuemart_product_id), 0) AS orderedquantity ';
		$q .= ' ,l.*, p.* ';
		$q .= ' FROM `#__virtuemart_products_'.VMLANG.'` as l';
		$q .= ' INNER JOIN `#__virtuemart_products` as p using (`virtuemart_product_id`) ';
		$q .= ' LEFT JOIN `#__virtuemart_purchaseordersproducts` as pop using (`virtuemart_product_id`) ';
		$q .= ' WHERE TRUE ';
		$q .= ' AND virtuemart_purchaseorders_id='. $ID;
		//~ if ($publishedOnly) {
			//~ $q .= 'AND `published`=1';
		//~ }
		
		//~ if ($ID = JRequest::getInt ('virtuemart_purchaseorders_id', 0)) {
			//~ $q .= ' and `virtuemart_purchaseorders_id`!=' . $ID;
		//~ }
		$this->_db->setQuery ($q);
		$result = $this->_db->loadObjectList ();

		$errMsg = $this->_db->getErrorMsg ();
		$errs = $this->_db->getErrors ();

		if (!empty($errMsg)) {
			$app = JFactory::getApplication ();
			$errNum = $this->_db->getErrorNum ();
			$app->enqueueMessage ('SQL-Error: ' . $errNum . ' ' . $errMsg);
		}

		if ($errs) {
			$app = JFactory::getApplication ();
			foreach ($errs as $err) {
				$app->enqueueMessage ($err);
			}
		}

		return $result;
	}
	
		
	function getPurchaseordersstateList(){
		$q  = 'SELECT `virtuemart_purchaseordersstate_id` , `pot_name` ';
		$q .= 'FROM #__virtuemart_purchaseordersstate ';
		$q .= 'WHERE virtuemart_purchaseordersstate_id in (5,6) ';
		$q .= 'ORDER BY virtuemart_purchaseordersstate_id';

		$this->_db->setQuery ($q);
		$result = $this->_db->loadObjectList ();

		$this->gettingSQLErrors();

		return $result;
	}
	
	
	
	
	
	/**
	 * Build category filter
	 *
	 * @return object List of category to build filter select box
	 */
	function getCategoryFilter(){
		$db = JFactory::getDBO();
		$query = 'SELECT `virtuemart_purchaseorders_id` as `value`, `po_name` as text'
				.' FROM #__virtuemart_purchaseorders_'.VMLANG.'`';
		$db->setQuery($query);

		$categoryFilter[] = JHTML::_('select.option',  '0', '- '. JText::_('COM_VIRTUEMART_SELECT_MANUFACTURER_CATEGORY') .' -' );

		$categoryFilter = array_merge($categoryFilter, (array)$db->loadObjectList());


		return $categoryFilter;

	}
	
	
	
     public function getOnePurchaseorders() {

     	if(empty($this->_data)){
     		$this->_data = $this->getTable('purchaseorders');
     		$this->_data->load($this->_id);
     	}

     	return $this->_data;
     }
     
     
     	/*funcion interna para dezplegar los errores de sql en la vista*/
	private function gettingSQLErrors() {

		$errMsg = $this->_db->getErrorMsg ();
		$errs = $this->_db->getErrors ();

		if (!empty($errMsg)) {
			$app = JFactory::getApplication ();
			$errNum = $this->_db->getErrorNum ();
			$app->enqueueMessage ('SQL-Error: ' . $errNum . ' ' . $errMsg);
		}

		if ($errs) {
			$app = JFactory::getApplication ();
			foreach ($errs as $err) {
				$app->enqueueMessage ($err);
			}
		}


	}
	
}

// pure php no closing tag
