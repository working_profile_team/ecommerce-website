<?php
/**
*
* Model
* 
* @author Gonzalo Sanchez Cano
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if(!class_exists('VmModel'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'vmmodel.php');

class VirtuemartModelPurchaseorders extends VmModel {

	
	function __construct() {
		parent::__construct('virtuemart_purchaseorders_id');
		$this->setMainTable('purchaseorders');
		$this->addvalidOrderingFieldName(array('po_name'));
		$config=JFactory::getConfig();
	}

	/**
	 * Delete all record ids selected
     *
     * @return boolean True is the remove was successful, false otherwise.
     */
	function remove($categoryIds)
	{
    	$table = $this->getTable('purchaseorders');

    	foreach($categoryIds as $categoryId) {
       		if($table->checkManufacturer($categoryId)) {
	    		if (!$table->delete($categoryId)) {
	            		vmError($table->getError());
	            		return false;
	       		}
       		}
       		else {
				vmError(get_class( $this ).'::remove '.$categoryId.' '.$table->getError());
       			return false;
       		}
    	}
    	return true;
	}



	function getPurchaseOrders($onlyPublished=false, $noLimit=false)
	{
		$this->_noLimit = $noLimit;
		$select = 'l.*, po.*, pot_name AS po_state FROM `#__virtuemart_purchaseorders_'.VMLANG.'` as l';
		$joinedTables  = ' JOIN `#__virtuemart_purchaseorders` as po using (`virtuemart_purchaseorders_id`)';
		$joinedTables .= ' LEFT JOIN `#__virtuemart_purchaseordersstate` as pos on pos.virtuemart_purchaseordersstate_id = po_stateid';
		$where = array();
		if ($onlyPublished) {
			$where[] = ' ';
		}

		$whereString = '';
		if (count($where) > 0) $whereString = ' WHERE '.implode(' AND ', $where) ;
		if ( JRequest::getCmd('view') == 'purchaseorders') {
			$ordering = $this->_getOrdering('po.');
		} else {
			$ordering = ' order by po_name DESC';
		}
		
		$this->_data = $this->exeSortSearchListQuery(0,$select,$whereString,$joinedTables,$ordering);

		return $this->_data;
	}


		function getProductsListByPurchaseOrder ($publishedOnly = FALSE) {
		//get the virtuemart_purchaseorders_id
		$ID = $this->_id;
		// get products
		$q = 	'SELECT IFNULL((SELECT SUM(orderedquantity) ';
		$q .= ' 		FROM #__virtuemart_purchaseordersproducts AS item ';
		$q .= ' 		WHERE item.virtuemart_purchaseorders_id = '.$ID.' AND ';
		$q .= ' 		item.virtuemart_product_id = p.virtuemart_product_id), 0) AS orderedquantity ';
		$q .= ' ,l.*, p.*, vc.* ';
		$q .= ' FROM `#__virtuemart_products_'.VMLANG.'` as l';
		$q .= ' INNER JOIN `#__virtuemart_products` as p using (`virtuemart_product_id`) ';
		$q .= ' INNER JOIN `#__virtuemart_vendors` as v using (`virtuemart_vendor_id`) ';
		$q .= ' INNER JOIN `#__virtuemart_currencies` as vc on vc.virtuemart_currency_id = v.vendor_currency ';
		$q .= ' LEFT JOIN `#__virtuemart_purchaseordersproducts` as pop using (`virtuemart_product_id`) ';
		$q .= ' WHERE TRUE ';
		$q .= ' AND virtuemart_purchaseorders_id='. $ID .' ';
		$q .= '	ORDER BY product_sku ';
		
		$this->_db->setQuery ($q);
		$result = $this->_db->loadObjectList ();

		$this->gettingSQLErrors();

		return $result;
	}
	
		
	function getAvailableProductsList(){
		$q  = "SELECT `virtuemart_product_id` , `product_sku` ";
		$q .= "FROM `#__virtuemart_products_".VMLANG."` as l ";
		$q .= "INNER JOIN `#__virtuemart_products` as p using (`virtuemart_product_id`) ";
		$q .= "WHERE product_sku IS NOT NULL AND product_sku != '' ";
		$q .= "ORDER BY product_sku ";

		$this->_db->setQuery ($q);
		$result = $this->_db->loadObjectList ();

		$this->gettingSQLErrors();

		return $result;
	}
	
	function getProduct(){
		$id = $_POST['product_id'];
		$q= '	SELECT ';
		$q .= ' 	l.*, p.*, vc.* ';
		$q .= ' FROM `#__virtuemart_products_'.VMLANG.'` as l';
		$q .= ' INNER JOIN `#__virtuemart_products` as p using (`virtuemart_product_id`) ';
		$q .= ' INNER JOIN `#__virtuemart_vendors` as v using (`virtuemart_vendor_id`) ';
		$q .= ' INNER JOIN `#__virtuemart_currencies` as vc on vc.virtuemart_currency_id = v.vendor_currency ';
		$q .= ' WHERE product_sku IS NOT NULL AND virtuemart_product_id = '. $id; 
		
		$db = JFactory::getDBO();
		$db->setQuery($q);
		$resultado = $db->loadAssocList();
		return json_encode( $resultado );
	}
	
	function removeProducts(){
		JRequest::checkToken() or jexit( 'Invalid Token save' );
		$data = JRequest::get('post');
		$db = JFactory::getDBO();
		for ($i = 0; $i < count($data['virtuemart_purchaseorders_id']); $i++) {
			$ID = $data['virtuemart_purchaseorders_id'][$i];
			$q = 	'DELETE FROM `#__virtuemart_purchaseordersproducts` WHERE virtuemart_purchaseorders_id='.$ID;
			$db->setQuery($q);
			$db->query();
		}
	}
	
	function getSaveProducts($data = 0){
		$ID = $data['virtuemart_purchaseorders_id'];
		if ($ID ==0){
			$ID = '(SELECT MAX(virtuemart_purchaseorders_id) FROM #__virtuemart_purchaseorders)';
		}
		//var_dump($this, $_POST, $_GET, $data);
		//die();
		JRequest::checkToken() or jexit( 'Invalid Token save' );
		if($data===0)$data = JRequest::get('post');
		$import_cost_total = strtok($data['po_import_cost_total'], " ");
		$currency = strtok(" ");

		$q = 	'DELETE FROM `#__virtuemart_purchaseordersproducts` WHERE virtuemart_purchaseorders_id='.$ID;
		$db = JFactory::getDBO();
		$db->setQuery($q);
		$db->query();
		

		for ($i = 0; $i < count($data['products-virtuemart_product_id']); $i++) {
			

			$q = 	'INSERT INTO  `#__virtuemart_purchaseordersproducts` (`virtuemart_purchaseorders_id`, virtuemart_product_id, orderedquantity) VALUES ';
			$q .= 	"(".$ID.", ".$data['products-virtuemart_product_id'][$i].", ".$data['products-orderedquantity'][$i].")";
			$db = JFactory::getDBO();
			$db->setQuery($q);
			$db->query();
		}
	}
	
	/**
	 * Build category filter
	 *
	 * @return object List of category to build filter select box
	 */
	function getCategoryFilter(){
		$db = JFactory::getDBO();
		$query = 'SELECT `virtuemart_purchaseorders_id` as `value`, `po_name` as text'
				.' FROM #__virtuemart_purchaseorders_'.VMLANG.'`';
		$db->setQuery($query);

		$categoryFilter[] = JHTML::_('select.option',  '0', '- '. JText::_('COM_VIRTUEMART_SELECT_MANUFACTURER_CATEGORY') .' -' );

		$categoryFilter = array_merge($categoryFilter, (array)$db->loadObjectList());


		return $categoryFilter;

	}
	
	/*funcion interna para dezplegar los errores de sql en la vista*/
	private function gettingSQLErrors() {

		$errMsg = $this->_db->getErrorMsg ();
		$errs = $this->_db->getErrors ();

		if (!empty($errMsg)) {
			$app = JFactory::getApplication ();
			$errNum = $this->_db->getErrorNum ();
			$app->enqueueMessage ('SQL-Error: ' . $errNum . ' ' . $errMsg);
		}

		if ($errs) {
			$app = JFactory::getApplication ();
			foreach ($errs as $err) {
				$app->enqueueMessage ($err);
			}
		}


	}

}

// pure php no closing tag
